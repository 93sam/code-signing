all:

test:
	$(MAKE) -C integration-tests test

clean:
	$(MAKE) -C integration-tests clean
